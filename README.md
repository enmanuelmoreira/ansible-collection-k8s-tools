# Ansible Collection: k8s-tools

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-collection-k8s-tools/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-collection-k8s-tools/-/commits/main)
[![Galaxy Collection][badge-collection]][link-galaxy]

This role installs a set of tools to manage Kubernetes clusters.

## Requirements

- Ansible > 2.9 <= 4.10.0

- github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Installation

Install via Ansible Galaxy:

```bash
ansible-galaxy collection install enmanuelmoreira.k8s_tools
```

Or include this collection in your playbook's requirements.yml file:

```bash
---
collections:
  - name: enmanuelmoreira.k8s_tools
```

You also can download the collection using the following commands:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-collection-k8s-tools
cd ansible-collection-k8s-tools
git submodule update --init --recursive
```

You could modify the example playbook `playbook-example.yml` on the section roles to install the software as you wish.

And run the playbook:

```bash
ansible-playbook playbook-example.yml -K
```

## K8s Tools Available

- [Helm](https://helm.sh): Helm is the best way to find, share, and use software built for Kubernetes.
- [kompose](https://kompose.io): Go from Docker Compose to Kubernetes.
- [krew](krew.sigs.k8s.io): Find and install kubectl plugins.
- [kubecolor](https://github.com/hidetatz/kubecolor): Colorizes kubectl output.
- [kubectx](https://kubectx.dev/): Faster way to switch between clusters and namespaces in kubectl.
- [kubectl](https://kubernetes.io/docs/tasks/tools/): The Kubernetes command-line tool, kubectl, allows you to run commands against Kubernetes clusters. You can use kubectl to deploy applications, inspect and manage cluster resources, and view logs.
- [kubergrunt](https://github.com/gruntwork-io/kubergrunt): Kubergrunt is a standalone go binary with a collection of commands to fill in the gaps between Terraform, Helm, and Kubectl.
- [kubescape](https://github.com/armosec/kubescape): Kubescape is a K8s open-source tool providing a multi-cloud K8s single pane of glass, including risk analysis, security compliance, RBAC visualizer and image vulnerabilities scanning.
- [kubeseal](https://github.com/bitnami-labs/sealed-secrets): A Kubernetes controller and tool for one-way encrypted Secrets.
- [kubeval](https://kubeval.com/): Validate your Kubernetes configuration files, supports multiple Kubernetes versions.
- [kustomize](https://kustomize.io/): Kustomize introduces a template-free way to customize application configuration that simplifies the use of off-the-shelf applications.
- [Lens](https://k8slens.dev/): A Graphical interface to manage Kubernetes Clusters.
- [OpenLens](https://github.com/MuhammedKalkan/OpenLens): A Graphical interface to manage Kubernetes Clusters. This build does not require you to log in and includes only open source part.

## Dependencies

None.

## Example Playbook

    - hosts: localhost
      gather_facts: yes
      become: yes

      vars_prompt:
        - name: github_token
          prompt: "What is your GitHub Token?"
          default: "{{ lookup('env','GITHUB_TOKEN') }}"
          private: yes
    
      roles:
        - helm
        - kompose
        - krew
        - kubecolor
        - kubectl
        - kubectx
        - kubergrunt
        - kubescape
        - kubeseal
        - kubeval
        - kustomize
        - lens

The GitHub Token is optional (until you're restricted for download from the GitHub API), you will be enter to bypassing input your token.

## License

MIT / BSD

[link-galaxy]: https://galaxy.ansible.com/enmanuelmoreira/k8s_tools
[badge-collection]: https://img.shields.io/badge/collection-enmanuelmoreira.k8s_tools-blue
